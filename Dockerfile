FROM python:3.9-slim-bullseye

# Install dependencies:
COPY requirements.txt .
RUN pip install -r ./requirements.txt

CMD ["python", "app.py"]